/*jshint browser: true, jquery: true*/
/*global  $, _, Backbone, Handlebars, app: true, Logger, d3, Benchmark, FB*/
(function() {
    'use strict';

    _.mixin(_.string.exports());



    String.prototype.toCamel = function(){
        return this.replace(/(\-[a-z])/g, function($1){return $1.toUpperCase().replace('-','');});
    };

    window.Logger = {};
    Logger.enabled = true;
    Logger.log = function(args) {
        var console = window.console;
        if (!Logger.enabled) {
            return;
        }
        if (console) {
            if (console.log && console.log.apply) {
                return console.log.apply(console, arguments);
            }
        }
    };

    jQuery.fn.removeClassRgxp = function() {
        var regexp = arguments[0] ? (arguments[0] && _.isRegExp(arguments[0]) ? arguments[0] : false) : false,
            classlist = [],
            new_classlist = [],
            new_class_string = "";
        if (regexp === false) {
            return $(this);
        }
        if (!$(this)) {
            return $(this);
        }
        if (!$(this).attr('class')) {
            return $(this);
        }
        classlist = $(this).attr('class').split(/\s+/);
        _.each(classlist, function(c) {
            if (!c.match(regexp)) {
                new_classlist.push(c);
            }
        });
        new_class_string = new_classlist.join(' ');
        $(this).attr('class', new_class_string);
        return $(this);
    };

    window.app = {
        base_url: window.base_url || '',
        api_url: window.base_url + 'api/',
        getApiUrl: function() {
            return this.api_url;
        },
        fragment: window.fragment || '',

        module: function() {
            // Internal module cache.
            var modules = {};
            // Create a new module reference scaffold or load an
            // existing module.
            return function(name) {
            // If this module has already been created, return it.

            if (name === '*') {
                return modules;
            }

            if (modules[name]) {
                return modules[name];
            }

            // Create a module and save it under this name
            return modules[name] = {

                name: 'default',
                Models: {},
                /**
                * @method getModel
                * @param: model The model to be returned
                * @param: [optional] create, Creates the widget if non existant
                */
                getModel: function() {
                    var model = arguments[0] || false,
                    create = arguments[1] || false,
                    instance_params = arguments[2] || '',
                    base_class = model.substr(0,1).toUpperCase() + model.substr(1);

                    if (!model) {
                        return;
                    } else {
                        if (this.Models[model]) {
                            return this.Models[model];
                        } else {
                            if (create && this.Models[base_class]) {
                                return this.Models[model] = new this.Models[base_class](instance_params);
                            }
                        }
                    }
                    return;
                },
                Views: {},
                /**
                * @method getWidget
                * @param: widget The widget to be returned
                * @param: [optional] create, Creates the widget if non existant
                */
                getWidget: function() {
                    var widget  = arguments[0] || false,
                    create = arguments[1]  || false,
                    instance_params = arguments[2] || {},
                    base_class = widget.substr(0,1).toUpperCase() + widget.substr(1).toCamel();

                    if (!widget) {
                        return;
                    } else {
                        if (this.Views[widget]) {
                            return this.Views[widget];
                        } else {
                            if (create && this.Views[base_class]) {
                                return this.Views[widget] = new this.Views[base_class](instance_params);
                            }
                        }
                    }
                    return;
                },
                Collections: {},
                /**
                * @method getCollection
                * @param: collection The collection to be returned
                * @param: [optional] create, Creates the widget if non existant
                */
                getCollection: function() {
                    var collection = arguments[0] || false,
                    create = arguments[1] || false,
                    instance_params = arguments[2] || '',
                    base_class = collection.substr(0,1).toUpperCase() + collection.substr(1);

                    if (!collection) {
                        return;
                    } else {
                        if (this.Collections[collection]) {
                            return this.Collections[collection];
                        } else {
                            if (create && this.Collections[base_class]) {
                                return this.Collections[collection] = new this.Collections[base_class](instance_params);
                            }
                        }
                    }
                    return;
                }
        };
    };
}()
};

    (function(w){
        var perfNow;
        var perfNowNames = ['now', 'webkitNow', 'msNow', 'mozNow'];
        if(!!w.performance) for(var i = 0; i < perfNowNames.length; ++i) {
            var n = perfNowNames[i];
            if(!!w.performance[n]) {
                perfNow = function() {
                    return w.performance[n]();
                };
                break;
            }
        }
        if(!perfNow)
        {
            perfNow = Date.now;
        }
        w.perfNow = perfNow;
    })(window);

    app.Models = {};
    app.Views = {};
    app.Collections = {};
    app.Routers = {};

    app.Models.M = Backbone.Model.extend({
        tryFetch: function() {
            var recursive = arguments[0] == 'r' || arguments[0] == 'recursive' ? true : false;
            if (app.module('fb').getAccessToken()) {
                if (this.setAuth) {
                    this.setAuth();
                }
                if (recursive && this.fetchR) {
                    this.fetchR();
                    return this;
                }
                this.fetch();
            }
            return this;
        }
    });

    app.Views.V = Backbone.View.extend({
        instanceOf:             "V",
        _subViews:              {},
        _region:                false,
        _rendered:              false,
        _templatingEngine:      "underscore",
        _startsHidden:          false,
        _subViewsStartHidden:   false,
        _subViewsInsertMode:    "appendTo",
        _subViewsContainer:     false,  // no container provided subViews are inserted to $el
        _perspective:           "nop",
        _perspective_options:   {},
        _parentView:            false,
        _paginate:              false,


        initialize: function() {
            if (this.init) {
                this.init(arguments);
            }
            this._perspective = this.options._perspective || 'nop';
            this._parentView = this.options._parentView || false;
            this.$el.removeClassRgxp(/^view\d+/);
            this.$el.addClass(this.cid);

            if (this.classList) {
                this.$el.addClass(this.classList.join(' '));
            }

            this.configSubViews();
            this.compileTemplates();
            this.bindCollection();
            this.bindModel();
            if (this.postInit) {
                this.postInit();
            }
        },

        //  creates a subView scaffolding
        //  or returns the requested subViews
        subViews: function(name) {
            var subViews = this._subViews,
                self = this;

            return function(name) {
                if (self._subViews[name]) {
                    return self._subViews[name];
                }
                self._subViews[name] = {
                    owner: self,
                    catalog: {},
                    views: {},
                    utils: {},
                    at: function(i) {
                        var array;
                        if (_.isObject(this.views)) {
                            array = _.toArray(this.views);
                            if (i < array.length) {
                                return array[i];
                            }
                        }
                    },
                    add: function(model) {
                        var subView,
                            $container,
                            container_selector = "";


                        if (!this.views[model.cid]) {
                            if (!this.catalog.baseClass) {
                                return this;
                            }
                            // creating the subView
                            subView =
                                this.views[model.cid] =
                                    new this.catalog.baseClass({ _parentView: this.owner, model: model });

                            // also adding the cid class here, in case the initialize function is overriden.
                            subView.$el.addClass(subView.cid);
                            if (this.catalog.startHidden) {
                                subView.$el.hide();
                            }


                            if (_.isObject(this.catalog.container)) {
                                if (this.catalog.container.tagName) {
                                    container_selector += this.catalog.container.tagName;
                                }
                                if (this.catalog.container.id) {
                                    container_selector += '#' + this.catalog.container.id;
                                }
                                if (    this.catalog.container.classList &&
                                        _.isArray(this.catalog.container.classList)
                                    ) {
                                    container_selector += '.' + this.catalog.container.classList.join('.');
                                }

                                $container = this.owner.$el.find(container_selector);
                                if ($container.length === 0) {
                                    $container = $('<' + (this.catalog.container.tagName ? this.catalog.container.tagName : 'div') + '>');
                                    $container.attr('id', this.catalog.container.id ? this.catalog.container.id : '');
                                    $container.addClass(_.isArray(this.catalog.container.classList) ? this.catalog.container.classList.join(' ') : '');
                                    $container[this.catalog.container.insertMode](this.owner.$el);
                                }
                            } else {
                                // the default subViews container is the $el itself
                                $container = this.owner.$el;
                            }

                            // if the subView is not found in the dom we insert
                            if (!$container.find('.' + subView.cid).length) {
                                if (_.isFunction(subView.$el[this.catalog.insertMode])) {
                                    subView.$el[this.catalog.insertMode]($container);
                                } else {
                                    Logger.log(this.instanceOf + '[render]', 'unknown subViewsInsertMode');
                                }
                            }
                            subView.render();
                        }
                        return this;
                    },
                    addAll: function() {
                        if (!this.catalog.collection) {
                            return;
                        }
                        this.catalog.collection.each(function(model) {
                            this.add(model);
                        }, this);
                    },
                    renderAll: function() {
                        if (!this.catalog.collection) {
                            return;
                        }
                        _.each(this.views, function(view) {
                            view.render();
                        });
                    },
                    remove: function() {

                    },
                    bindCollection: function() {
                        this.catalog.collection.on('add', this.add, this);
                        this.catalog.collection.on('remove', this.remove, this);
                        return this;
                    },
                    show: function() {
                        var timing = arguments[0] ? arguments[0] : false;
                        this.owner.show(timing ? timing : '');
                        _.each(this.views, function(subView) {
                            if (subView.show) {
                                subView.show(timing ? timing : '');
                            } else {
                                subView.$el.show(timing ? timing : '');
                            }
                        }, this);
                        return this;
                    },
                    hide: function() {
                        var timing = arguments[0] ? arguments[0] : false;
                        _.each(this.views, function(subView) {
                            if (subView.hide) {
                                subView.hide(timing ? timing : '');
                            } else {
                                subView.$el.hide(timing ? timing : '');
                            }
                        }, this);
                        return this;
                    },
                    hideOthers: function() {
                        _.each(this.owner._subViews, function(subViews) {
                            if (this !== subViews) {
                                subViews.hide(arguments);
                            }
                        }, this);
                        return this;
                    }
                };
                return self._subViews[name];
            } (name);
        },

        configSubViews: function() {
            if (this.subViewsConf) {

                _.each(this.subViewsConf, function(conf, key, list) {
                    var s;

                    this._subViews = {};
                    s = this.subViews(key);
                    s.catalog = conf.catalog ? conf.catalog : false;
                    if (
                        !s.catalog.collection ||
                        s.catalog.collection === 'default' ||
                        ((s.catalog.collection && this.collection) && s.catalog.collection.cid !== this.collection)
                    ) {
                        if (this.collection) {
                            s.catalog.collection = this.collection;
                        }
                    }

                    if (conf.utils) {
                        _.extend(s.utils, conf.utils);
                    }
                    s.bindCollection();
                }, this);
            }

            return this;
        },

        _getTemplateCompiler: function() {
            var compiler;
            if (this._templatingEngine == 'underscore') {
                compiler = _.template;
            } else if (this._templatingEngine == 'handlebars') {
                compiler = Handlebars.compile;
            } else {
                Logger.log(this.instanceOf, '[compileTemplates]', 'unknown templating engine');
                return;
            }
            return compiler;
        },

        compileTemplate: function() {
            var compiler = this._getTemplateCompiler();
            if (!compiler) {
                return;
            }
            if (this.template && $(this.template).length) {
                this.c_tmpl = compiler($(this.template).html());
            }
            return this;
        },

        compileTemplates: function() {
            var compiler = this._getTemplateCompiler();
            if (!compiler) {
                return;
            }
            if (_.isObject(this.templates)) {
                if (!this.c_tmpls) {
                    this.c_tmpls = {};
                }
                _.each(this.templates, function(template) {
                    var $template;
                    if (!(_.isObject(template) && template.id && _.isString(template.id))) {
                        return;
                    }
                    $template = $(template.id);
                    if ($template.length) {
                        template.compiled = compiler($template.html());
                    }
                }, this);
            }
            this.compileTemplate();
            return this;
        },

        /**
        * Builds the JSON string to be
        * passed to the render function.
        * Override this method to provide
        * different serialization logic.
        *
        * @return {String} The JSON string
        * to be used with the compiled template
        */
        serialize: function() {
            if (this.model) {
                return this.model.toJSON();
            } else {
                return {};
            }
        },

        perpectives: {
            'nop': 'nop'
        },

        undelegatePerspective: function() {

        },

        undelegatePerspectives: function() {
            if (!this.perspecives) {
                return this;
            }
            if (!this.perspectives.nop) {
                this.perpectives.nop = 'nop';
            }
            this.setPerspective('nop');
        },

        delegatePerspective: function() {

        },

        isPerspective: function(p) {
            if (!this.perspectives) {
                return false;
            }
            if (this.perspectives[p] && _.isFunction(this.perspectives[p])) {
                return true;
            }
        },

        setPerspective: function(p) {
            var _perspective_options = arguments[1] ? arguments[1] : {};
            if (!(this._perspective == p && this.isPerspective(p))) {
                this._perspective_options = _perspective_options;
                // this.undelegatePerspectives();
                // this.delegatePerspective(p);
                this._perspective = p;
            }
            return this;
        },

        getPerspective: function() {
            return this._perspective;
        },

        displayPerspective: function() {
            if (    _.isString(this._perspective)   &&
                    this[this._perspective]         &&
                    _.isFunction(this[this._perspective])
                ) {
                this[this._perspective](this._perspective_options);
            }
        },

        nop: function() {
            return this;
        },

        render: function() {
            if (this.model) {
                if (this.c_tmpl) {
                    if (this._startsHidden) {
                        this.$el.hide();
                    }

                    // TODO: refactor the region case
                    if (this._region) {
                        if (!this.$el.find(this._region).length) {
                            this.$el.prepend(this.c_tmpl(this.serialize()));
                        } else {
                            this.$el.find(this._region).html(this.c_tmpl(this.serialize()));
                        }
                    } else {
                        this.$el.html(this.c_tmpl(this.serialize()));
                    }

                }
            }
            if (!_.isUndefined(this.id) && _.isString(this.id)) {
                if (!this._rendered) {
                    this._rendered = true;
                    if (this.c_tmpl) {
                        this.$el.html(this.c_tmpl(this.serialize()));
                    }
                }
            } else if (this.className && $(this._parentView.cid).length) {
                // TODO: review this scope

                if (!$(this._parentView._subViewsContainer + ' ' + this.className).hasClass(this.cid)) {
                    this.$el[this._parentView._subViewsInsertMode](this._parentView._subViewsContainer);
                }
            }

            ////////////////
            //// paginator
            ////////////////

            if (this._paginate && this.collection) {
                this.createPaginator();
            }
            return this;
        },

        createPaginator: function() {
            var $container = this.$el;
            if (!_.isObject(this.paginator)) {
                this.paginator = new app.Views.Paginator({
                    _parentView: this,
                    collection: this.collection
                });
                if (!$container.find('.' + this.paginator.cid).length) {
                    if (_.isFunction(this.paginator.$el[this.paginator.insertMode])) {
                        this.paginator.$el[this.paginator.insertMode]($container);
                    } else {
                        Logger.log(this.instanceOf + '[render]', 'unknown subViewsInsertMode');
                    }
                    this.paginator.render();
                }
            }
        },

        isRendered: function() {
            return this._rendered;
        },

        subViewsLength: function() {
            var array;
            if (_.isObject(this.subViews)) {
                array = _.toArray(this.subViews);
                return array.length;
            }
        },

        subViewsAt: function(i) {
            var array;
            if (_.isObject(this.subViews)) {
                array = _.toArray(this.subViews);
                if (i < array.length) {
                    return array[i];
                }
            }
        },

        show: function() {
            var timing = arguments[0] ? arguments[0] : false;
            this.$el.show(timing ? timing : '');
            return this;
        },

        hide: function() {
            var timing = arguments[0] ? arguments[0] : false;
            this.$el.hide(timing ? timing : '');
            return this;
        },

        bindModel: function() {
            if (_.isObject(this.model)) {
                this.model.on("change",     this.render, this);
                this.model.on("destroy",    this.remove, this);
            }
            return this;
        },

        bindCollection: function() {
            if (_.isObject(this.collection)) {
                // this.collection.on("add",                this.addSubView, this);
                // this.collection.on("remove",         this.removeSubView, this);
                this.collection.on("reset",             this.render, this);
                this.collection.on('add remove reset',  this.displayPerspective, this);
            }
            return this;
        },

        addSubView: function(model) {
            var subView,
                $container;

            if (!this.subViews[model.cid]) {
                if (!this.subViewBase) {
                    return this;
                }
                // creating the subView
                subView =
                    this.subViews[model.cid] =
                        new this.subViewBase({ _parentView: this, model: model });

                // also adding the cid class here, in case the initialize function is overriden.
                subView.$el.addClass(subView.cid);
                if (this._subViewsStartHidden) {
                    subView.$el.hide();
                }
                // subView is prepared

                if (this._subViewsContainer) {
                    $container = this.$el.find(this._subViewsContainer);
                    if (!$container.find('.' + subView.cid).length) {
                        if (_.isFunction(subView.$el[this._subViewsInsertMode])) {
                            subView.$el[this._subViewsInsertMode]($container);
                        } else {
                            Logger.log(this.instanceOf + '[render]', 'unknown subViewsInsertMode');
                        }
                    }
                } else {
                    $container = this.$el;
                    if (!$container.find('.' + subView.cid).length) {
                        if (_.isFunction(subView.$el[this._subViewsInsertMode])) {
                            subView.$el[this._subViewsInsertMode]($container);
                        } else {
                            Logger.log(this.instanceOf + '[render]', 'unknown subViewsInsertMode');
                        }
                    }
                }
                subView.render();
            }
            return this;
        },

        removeSubView: function(model) {
            var view = this.subViews[model.cid] || false;

            if (view) {
                if (view.remove) {
                    view.remove();
                }
                delete this.subViews[model.cid];
            }
            return this;
        },
        getSubView: function(id) {
            var model;
            if (_.isNumber(id)) {
                id = id + "";
            }

            if (!this.collection) {
                return;
            }
            model = this.collection.get(id);
            if (_.isObject(model) && this.subViews[model.cid]) {
                return this.subViews[model.cid];
            }
            return;
        },

        /**
        * Find a subView that has the
        * given view cid
        * @return {[type]} [description]
        */
        getSubViewByCid: function(cid) {
            return _.find(this.subViews, function(subView) {
                return subView.cid = cid;
            }, this);
        },

        renderSubViews: function() {
            _.each(this.subViews, function(subView) {
                subView.render();
            }, this);
            return this;
        },
        showSubViews: function() {
            var timing = arguments[0] ? arguments[0] : false;
            this.show(timing ? timing : '');
            _.each(this.subViews, function(subView) {
                if (subView.show) {
                    subView.show(timing ? timing : '');
                } else {
                    subView.$el.show(timing ? timing : '');
                }
            }, this);
            return this;
        },
        showSubView: function(id) {
            var model,
                timing = arguments[0] ? arguments[0] : false;
            if (!this.collection) {
                return this;
            }
            model = this.collection.get(id);
            if (_.isObject(model) && this.subViews[model.cid]) {
                this.subViews[model.cid].show(timing ? timing : '');
            }
            return this;
        },
        hideSubViews: function() {
            var timing = arguments[0] ? arguments[0] : false;
            _.each(this._subViews, function(subView) {
                if (subView.hide) {
                    subView.hide(timing ? timing : '');
                }
            }, this);
            return this;
        },
        remove: function() {
            this.$el.remove();
        }
    });


    app.Collections.C = Backbone.Collection.extend({
        instanceOf: 'V',
        persist: false,
        paginate: false,
        initialize: function(models, options) {
            this.cid = _.uniqueId('collection');
            if (this.init) {
                this.init(arguments);
            }
        },
        tryFetch: function() {
            var recursive = arguments[0] == 'r' || arguments[0] == 'recursive' ? true : false;
            if (app.module('fb').getAccessToken()) {
                if (this.setAuth) {
                    this.setAuth();
                }
                if (recursive && this.fetchR) {
                    this.fetchR();
                    return this;
                }
                this.fetch({ add: true });
            }
            return this;
        }
    });

    app.Views.Paginator = app.Views.V.extend({
        className:      'paginator',
        template:       '#tmpl_paginator',
        insertMode:     'prependTo',
        events: {
            'click a.first': 'gotoFirst',
            'click a.prev': 'gotoPrev',
            'click a.next': 'gotoNext',
            'click a.last': 'gotoLast',
            'click a.page': 'gotoPage',
            'click .howmany a': 'changeCount',
            'click a.sortAsc': 'sortByAscending',
            'click a.sortDsc': 'sortByDescending',
            'click a.filter': 'filter'
        },

        initialize: function() {
            this.collection.on('reset add change', this.render, this);
            // this.collection.on('change', this.render, this);
            this._super('initialize', arguments);
        },

        render: function() {
            this.$el.html(this.c_tmpl(this.collection.info()));
            return this;
        },

        gotoFirst: function (e) {
            e.preventDefault();
            this.collection.goTo(1);
            return this;
        },

        gotoPrev: function (e) {
            e.preventDefault();
            this.collection.previousPage();
            return this;
        },

        gotoNext: function (e) {
            e.preventDefault();
            this.collection.nextPage();
            return this;
        },

        gotoLast: function (e) {
            e.preventDefault();
            this.collection.goTo(this.collection.information.lastPage);
            return this;
        },

        gotoPage: function (e) {
            e.preventDefault();
            var page = $(e.target).text();
            this.collection.goTo(page);
            return this;
        },

        changeCount: function (e) {
            e.preventDefault();
            var per = $(e.target).text();
            this.collection.howManyPer(per);
            return this;
        },

        sortByAscending: function (e) {
            e.preventDefault();
            var currentSort = this.getSortOption();
            this.collection.setSort(currentSort, 'asc');
            this.collection.pager();
            this.preserveSortOption(currentSort);
            return this;
        },

        getSortOption: function () {
            return $('#sortByOption').val();
        },

        preserveSortOption: function (option) {
            $('#sortByOption').val(option);
        },

        sortByDescending: function (e) {
            e.preventDefault();
            var currentSort = this.getSortOption();
            this.collection.setSort(currentSort, 'desc');
            this.collection.pager();
            this.preserveSortOption(currentSort);
            return this;
        },

        getFilterField: function () {
            return $('#filterByOption').val();
        },

        getFilterValue: function () {
            return $('#filterString').val();
        },

        preserveFilterField: function (field) {
            $('#filterByOption').val(field);
        },

        preserveFilterValue: function (value) {
            $('#filterString').val(value);
        },

        filter: function (e) {
            var fields = this.getFilterField(),
                filter = this.getFilterValue();

            e.preventDefault();

            this.collection.setFilter(fields, filter);
            this.collection.pager();
            this.preserveFilterField(fields);
            this.preserveFilterValue(filter);
        }
    });

    app.Routers.R = Backbone.Router.extend({
        go: function() {
            return this.navigate(_.toArray(arguments).join("/"), true);
        }
    });



    app.getModel = function(name) {
        var module_name,
            model_name,
            module,
            model,
            parts,
            modules;
        if (!name) {
            return;
        }

        // if the name is given in module.model notation
        if (/\./.test(name)) {
            parts = name.split('.');
            if (parts.length < 2) {
                return;
            }
            module_name = parts[0];
            model_name = parts[1];
            module = app.module(module_name).name;
            if (module !== 'default') {
                if (!module.getModel) {
                    return;
                }
                model = module.getModel(model_name);
                if (model) {
                    return model;
                } else {
                    return;
                }
            }
        }

        // searching all the modules for a model with the requested
        // name

        modules = app.module('*');
        model = undefined;
        model = _.find(modules, function(module) {
            var m;
            if (!module.getModel) {
                return;
            }
            m = module.getModel(name);
            if (m) {
                return m;
            } else {
                return;
            }
        });
        if (!model) {
            return;
        } else {
            return model.getModel(name);
        }
    };

    app.getCollection = function(name) {
        var module_name,
            collection_name,
            module,
            collection,
            parts,
            modules;
        if (!name) {
            return;
        }

        // if the name is given in module.collection notation
        if (/\./.test(name)) {
            parts = name.split('.');
            if (parts.length < 2) {
                return;
            }
            module_name = parts[0];
            collection_name = parts[1];
            module = app.module(module_name).name;
            if (!module.getCollection) {
                return;
            }
            if (module !== 'default') {
                collection = module.getCollection(collection_name);
                if (collection) {
                    return collection;
                } else {
                    return;
                }
            }
        }

        // searching all the modules for a model with the requested
        // name

        modules = app.module('*');
        collection = undefined;
        module = _.find(modules, function(module) {
            var c;
            if (!module.getCollection) {
                return;
            }
            c = module.getCollection(name);
            if (c) {
                return c;
            } else {
                return;
            }
        });
        if (!module) {
            return;
        } else {
            return module.getCollection(name);
        }
    };

    app.getView = function(name) {
        var module_name,
            view_name,
            module,
            view,
            parts,
            modules;
        if (!name) {
            return;
        }

        // if the name is given in module.model notation
        if (/\./.test(name)) {
            parts = name.split('.');
            if (parts.length < 2) {
                return;
            }
            module_name = parts[0];
            view_name = parts[1];
            module = app.module(module_name).name;
            if (module !== 'default') {
                if (!module.getWidget) {
                    return;
                }
                view = module.getWidget(view_name);
                if (view) {
                    return view;
                } else {
                    return;
                }
            }
        }

        // searching all the modules for a model with the requested
        // name

        modules = app.module('*');
        view = undefined;
        module = _.find(modules, function(module) {
            var v;
            if (!module.getWidget) {
                return;
            }
            v = module.getWidget(name);
            if (v) {
                return v;
            } else {
                return;
            }
        });
        if (!module) {
            return;
        } else {
            return module.getWidget(name);
        }
    };

    app.getWidget = function(name) {
        return app.getView(name);
    };

    app.get = function(name) {
        var o;
        o = app.getModel(name);
        if (o) {
            return o;
        }
        o = app.getView(name);
        if (o) {
            return o;
        }
        o = app.getCollection(name);
        if (o) {
            return o;
        }
        return;
    };

    app = _.extend(app, Backbone.Events);


    app._configuration = function _configuration() {
        var _conf = {
                dataset: {
                    greedy: false
                }
            },
            modify = _.isBoolean(arguments[0]) ? arguments[0] : false;
        if (modify) {
            return _conf;
        } else {
            return _.clone(_conf);
        }
    };
    /*
    * @method configure
    * @summary App-wide configuration
    * will happen here
    */
    app.configure = function configure() {
        var _configuration,
            module_name = arguments[0] && _.isString(arguments[0]) ? arguments[0] : false,
            settings = arguments[1] ? arguments[1] : false;

        //TODO: implement..

        return app._configuration();
    };

    app._route = undefined;
    app._route_params = undefined;
    app.setRouteParams = function(params) {
        if (!params) {
            return;
        }
        this._route_params = params;
    };

    app.getRouteParams = function() {
        return this._route_params;
    };

    app.setRoute = function() {
        var route = arguments[0] || false,
            params  = arguments[1] || false;

        if (!route) {
            return;
        }
        this._route = route;
        if (params) {
            this.setRouteParams(params);
        }
    };





//    _____ ____  ____  ___________   ________   ___    __    __________  ____  ____________  ____  ________
//   / ___// __ \/ __ \/_  __/  _/ | / / ____/  /   |  / /   / ____/ __ \/ __ \/  _/_  __/ / / /  |/  / ___/
//   \__ \/ / / / /_/ / / /  / //  |/ / / __   / /| | / /   / / __/ / / / /_/ // /  / / / /_/ / /|_/ /\__ \
//  ___/ / /_/ / _, _/ / / _/ // /|  / /_/ /  / ___ |/ /___/ /_/ / /_/ / _, _// /  / / / __  / /  / /___/ /
// /____/\____/_/ |_| /_/ /___/_/ |_/\____/  /_/  |_/_____/\____/\____/_/ |_/___/ /_/ /_/ /_/_/  /_//____/
//


    app.sort = {
        util: {
            swap: function swap(items, firstIndex, secondIndex){
                var temp = items[firstIndex];
                items[firstIndex] = items[secondIndex];
                items[secondIndex] = temp;
            }
        },


        insertionSort: function insertionSort(arr) {
           for (var i = 0, j, tmp; i < arr.length; ++i) {
              tmp = arr[i];
              for (j = i - 1; j >= 0 && arr[j] > tmp; --j) {
                 arr[j + 1] = arr[j];
             }
              arr[j + 1] = tmp;
           }
           return arr;
        },



        gnomeSort: function gnomeSort(arr) {
           var  i = 0,
                tmp;
           while (i < arr.length) {
              if (i === 0 || arr[i-1] <= arr[i]) {
                 i++;
              } else {
                 tmp = arr[i];
                 arr[i] = arr[i-1];
                 arr[--i] = tmp;
              }
           }
           return arr;
        },



        shellSort: function shellSort(arr) {
            var inc = Math.floor(parseInt(arr.length / 2, 10)),
                i, j,
                temp;

            while(inc > 0) {
                for(i = inc; i < arr.length; i++){
                    temp = arr[i];
                    j = i;
                    while(j >= inc && arr[j - inc] > temp) {
                        arr[j] = arr[j - inc];
                        j -= inc;
                    }
                    arr[j] = temp;
                }
                inc = Math.floor(parseInt(inc / 2.2, 10));
            }
            return arr;
        },

        //
        //
        //
        //
        //
        //

        /**
         * [pigeonholeSort description]
         * @todo: complete it
         * @param  {[type]} arr [description]
         * @return {[type]}     [description]
         */
        pigeonholeSort: function pigeon_sort(arr) {
            var min,
                max,
                i,
                res,
                num,
                d = [];
            //search min and max
            min = max = arr[0];
            for (i = 0; i < arr.length; i++) {
                num = arr[i];
                if (num < min) {
                    min = num;
                }
                if (num > max) {
                    max = num;
                }
            }

            for (i = 0; i < arr.length; i++) {
                num = arr[i];
                d[num-min]++;
            }

            // for ($i = 0; $i <= max - min; $i++ ) {
            //     while ($ + min]-- > 0) {
            //        $res[] = $i+min;
            //     }
            // }
            return res;
        },

        /**
         * [countingSort description]
         * @param  {[type]} arr [description]
         * @return {[type]}     [description]
         */
        countingSort: function(arr) {
            var i, j, k,
                min,
                max,
                newarray,
                counts;

            // find min, max to set range of counts array
            min = arr[0];
            max = arr[0];
            for(i=0; i<arr.length; i++) {
              if (min > arr[i])
                min = arr[i];
              else if (max < arr[i])
                max = arr[i];
            }
            counts = [];
            newarray = [];
            // initialize counts array
            for(i=0; i<max-min+1; i++) {
              counts[i] = 0;
            }
            // count distinct values from input array array
            for(i=0; i<arr.length; i++) {
              counts[arr[i]-min]++;
            }
            k = 0;
            // write output values ordered by counts index
            // replicate output value according to repeat count
            for(i=0; i<counts.length; i++) {
              for(j=0; j<counts[i]; j++) {
                newarray[k++] = i + min;
              }
            }
            return newarray;
        },

        /**
         * [heapSort description]
         * @param  {[type]} arr [description]
         * @return {[type]}     [description]
         */
        heapSort: function heapSort(arr) {
            var maxHeapify,
                buildMaxHeap,
                sort,
                i,
                heapSize;

                maxHeapify = function(arr, i) {
                    // var
                };




                buildMaxHeap = function(arr) {
                    heapSize = arr.length;
                    for (i = Math.floor(arr.length / 2); i <= 1; i--) {
                        maxHeapify(arr, 1);
                    }
                };

                buildMaxHeap(arr);
                for (i = arr.length; arr >= 2; i--) {
                    app.sort.util.swap(arr, 1, i);
                    heapSize = heapSize - 1;
                    maxHeapify(arr, 1);
                }



        },




        /**
         * [quickSort description]
         * @param  {[type]} arr [description]
         * @return {[type]}     [description]
         */
        quickSort: function quickSort(arr) {
            var less = [],
                greater = [];
          // return if array is unsortable
          if (arr.length <= 1){
            return arr;
          }



          // select and remove a pivot value pivot from array
          // a pivot value closer to median of the dataset may result in better performance
          var pivotIndex = Math.floor(arr.length / 2);
          var pivot = arr.splice(pivotIndex, 1)[0];

          // step through all array elements
          for (var x = 0; x < arr.length; x++){

            // if (current value is less than pivot),
            // OR if (current value is the same as pivot AND this index is less than the index of the pivot in the original array)
            // then push onto end of less array
            if (
              (arr[x] < pivot)           ||
              (arr[x] == pivot && x < pivotIndex)  // this maintains the original order of values equal to the pivot
            ){
              less.push(arr[x]);
            }

            // if (current value is greater than pivot),
            // OR if (current value is the same as pivot AND this index is greater than or equal to the index of the pivot in the original array)
            // then push onto end of greater array
            else {
              greater.push(arr[x]);
            }
          }

          // concatenate less+pivot+greater arrays
          return quickSort(less).concat([pivot], quickSort(greater));
        },

        /**
         * A selection sort implementation in JavaScript. The array
         * is sorted in-place.
         * @param {Array} items An array of items to sort.
         * @return {Array} The sorted array.
         */
        selectionSort: function selectionSort(items){

            var len = items.length,
                min, i, j,
                swap;

            /**
             * Swaps two values in an array.
             * @param {Array} items The array containing the items.
             * @param {int} firstIndex Index of first item to swap.
             * @param {int} secondIndex Index of second item to swap.
             * @return {void}
             */
            swap = function swap(items, firstIndex, secondIndex){
                var temp = items[firstIndex];
                items[firstIndex] = items[secondIndex];
                items[secondIndex] = temp;
            };

            for (i=0; i < len; i++){

                //set minimum to this position
                min = i;

                //check the rest of the array to see if anything is smaller
                for (j=i+1; j < len; j++){
                    if (items[j] < items[min]){
                        min = j;
                    }
                }

                //if the minimum isn't in the position, swap it
                if (i != min){
                    swap(items, i, min);
                }
            }

            return items;
        },


        /**
         * A bubble sort implementation in JavaScript. The array
         * is sorted in-place. This uses two reversed loops that
         * count down instead of counting up.
         * @param {Array} items An array of items to sort.
         * @return {Array} The sorted array.
         */
        bubbleSort: function bubbleSort(items){
            var len = items.length,
                i, j,
                swap;

            /**
             * Swaps two values in an array.
             * @param {Array} items The array containing the items.
             * @param {int} firstIndex Index of first item to swap.
             * @param {int} secondIndex Index of second item to swap.
             * @return {void}
             */
            swap = function swap(items, firstIndex, secondIndex){
                var temp = items[firstIndex];
                items[firstIndex] = items[secondIndex];
                items[secondIndex] = temp;
            };

            for (i=len-1; i >= 0; i--){
                for (j=len-i; j >= 0; j--){
                    if (items[j] < items[j-1]){
                        swap(items, j, j-1);
                    }
                }
            }

            return items;
        },



        /**
         * Sorts an array in ascending natural order using
         * merge sort. (Recursive)
         * @param {Array} items The array to sort.
         * @return {Array} The sorted array.
         */
        mergeSort: function mergeSort(items, recursive){
            if (items.length == 1) {
                return items;
            }

            var merge,
                middle = Math.floor(items.length / 2),
                left    = items.slice(0, middle),
                right   = items.slice(middle),
                work;

            if (recursive === true) {


                /**
                 * Merges to arrays in order based on their natural
                 * relationship.
                 * @param {Array} left The first array to merge.
                 * @param {Array} right The second array to merge.
                 * @return {Array} The merged array.
                 */
                merge = function merge(left, right){
                    var result = [];

                    while (left.length > 0 && right.length > 0){
                        if (left[0] < right[0]){
                            result.push(left.shift());
                        } else {
                            result.push(right.shift());
                        }
                    }

                    return result.concat(left).concat(right);
                };






                return merge(mergeSort(left), mergeSort(right));

            } else {
                /**
                 * Merges to arrays in order based on their natural
                 * relationship.
                 * @param {Array} left The first array to merge.
                 * @param {Array} right The second array to merge.
                 * @return {Array} The merged array.
                 */
                merge = function merge(left, right){
                    var result = [];

                    while (left.length > 0 && right.length > 0){
                        if (left[0] < right[0]){
                            result.push(left.shift());
                        } else {
                            result.push(right.shift());
                        }
                    }

                    result = result.concat(left).concat(right);

                    //make sure remaining arrays are empty
                    left.splice(0, left.length);
                    right.splice(0, right.length);

                    return result;
                };

                /**
                 * Sorts an array in ascending natural order using
                 * merge sort.
                 * @param {Array} items The array to sort.
                 * @return {Array} The sorted array.
                 */


                work = [];
                for (var i=0, len=items.length; i < len; i++){
                    work.push([items[i]]);
                }
                work.push([]);  //in case of odd number of items

                for (var lim=len; lim > 1; lim = Math.floor((lim+1)/2)){
                    for (var j=0,k=0; k < lim; j++, k+=2){
                        work[j] = merge(work[k], work[k+1]);
                    }
                    work[j] = [];  //in case of odd number of items
                }

                return work[0];
            }
        }

    };



    (function(app, Main) {
        Main.Models.DataPoint = Backbone.Model.extend({
            initialize: function(point) {
                this.set({
                    x: point.x,
                    y: point.y,
                    id: point.id
                });
            }
        });

        Main.Collections.DataSeries = Backbone.d3.PlotCollection.extend({
            model : Main.Models.DataPoint,
            // url : "bardata.json",
            fetch: function(){
              // No op
              // console.log('fetching is a no op in this example');
            }
        });

        Main.Views.Main = app.Views.V.extend({
            el: '.main-container',
            graphs: {},

            initialize: function() {

            },

            render: function(subpage) {
                this.subpage = subpage;

                $('.tmp').html($('#tmpl-' + subpage).html());
                $('#content h2.alpha').text($('.tmp').find('.title').text());
                $('#content .can').html($('.tmp').find('.content').html());
                $('#sidebar li').removeClass('active');
                $('#sidebar li:has(a[href=#' + subpage + '])').addClass('active');

                $('.tmp').html('');

                if (subpage === 'intro') {
                    this.graph();
                }
            },


            events: {
                'keyup .sort-name': 'sortName'
            },

            sortName: function(e) {
                var $target = $(e.target),
                    bench = new Benchmark.Suite(),
                    time,
                    timing,
                    result;

                time = function() {
                    var start,
                        end;

                    start = window.perfNow();
                    result = app.sort[$target.data('algo')]($target.val().split(''));
                    end = window.perfNow();
                    return end - start;
                };

                timing = time();

                // bench.add('bubbleSort', function() {
                //     app.sort[$target.data('algo')]($target.val().split(''));
                // });

                // bench.on('complete', function() {
                //     return this;
                // });



                $target.siblings('.js-result').text(result.join(''));
                $target.siblings('.js-result-length').text($target.val().length);
                $target.siblings('.js-result-timing').text(timing + ' ms');
                this.$el.find();
            },

            shuffle: function shuffle(array) {
              var i = array.length, j, t;
              while (--i > 0) {
                j = ~~(Math.random() * (i + 1));
                t = array[j];
                array[j] = array[i];
                array[i] = t;
              }
              return array;
            },

            graph: function() {
                var a;
                // $('.chart').each(_.bind(function(i, el) {
                //     var $el = $(el),
                //         algo = $(el).data('algorithm'),
                //         j = 0;
                //     this.graphs[algo] = {};
                //     this.graphs[algo].plot = new Backbone.d3.Canned.Line.View(new Main.Collections.DataSeries(),
                //                                                                         {
                //                                                                             div: '#chart-' + algo
                //                                                                         });

                //     this.graphs[$(el).data('algorithm')].plot.collection.add(_.map(_.shuffle(_.range(1, 100)), function (value, key) {
                //         return new Main.Models.DataPoint({
                //             x: key,
                //             y: value
                //         });

                //     }, this));


                // }, this));


                this.series1 = new Main.Collections.DataSeries();
                this.graphs = new Backbone.d3.Canned.Bar.View(this.series1, {div: '#chart-bubbleSort'});
                this.graphs.collection.reset([new Main.Models.DataPoint({x:0, y:0})]);

                a = _.map(_.shuffle(_.range(1, 100)), function (value, key) {
                    return new Main.Models.DataPoint({
                        x: key,
                        y: value
                    });
                }, this);

                this.graphs.collection.reset([
                  new Main.Models.DataPoint({x:1, y:6, id:1}),
                  new Main.Models.DataPoint({x:2, y:4, id:2}),
                  new Main.Models.DataPoint({x:3, y:2, id:3}),
                  new Main.Models.DataPoint({x:4, y:4, id:4}),
                  new Main.Models.DataPoint({x:5, y:6, id:5})
                ]);

                // this.graphs.collection.add(_.map(_.shuffle(_.range(1, 100)), function (value, key) {
                //     return new Main.Models.DataPoint({
                //         x: key,
                //         y: value
                //     });
                // }, this));


               // _.each(_.range(1,20), _.bind(function(i, ii, l) {
               //    setTimeout(_.bind(function() {
               //      this.graphs.collection.add(new Main.Models.DataPoint({y:Math.sin(i/10 * Math.PI), x: i}));
               //    }, this), 1 + Math.random() * 10000);
               //  },this));
            }
        });

    })(app, app.module('main'));




    //     ____  ____  __  ___________________
    //    / __ \/ __ \/ / / /_  __/ ____/ ___/
    //   / /_/ / / / / / / / / / / __/  \__ \
    //  / _, _/ /_/ / /_/ / / / / /___ ___/ /
    // /_/ |_|\____/\____/ /_/ /_____//____/
    app.Router = Backbone.Router.extend({
        routes: {
            "":                     "welcome",
            "welcome":              "welcome",
            "intro":                "intro",
            "tasks":                "tasks",
            "process":              "process",
            "evaluation":           "evaluation",
            "conclusion":           "conclusion",
            "teachers-page":        "teachersPage"
        },

        welcome: function() {
            app.module('main').getWidget('main', true).render('welcome');
        },

        intro: function() {
            app.module('main').getWidget('main', true).render('intro');
        },

        tasks: function() {
            app.module('main').getWidget('main', true).render('tasks');
        },

        process: function() {
            app.module('main').getWidget('main', true).render('process');
        },

        evaluation: function() {
            app.module('main').getWidget('main', true).render('evaluation');
        },

        conclusion: function() {
            app.module('main').getWidget('main', true).render('conclusion');
        },

        teachersPage: function() {
            app.module('main').getWidget('main', true).render('teachers-page');
        }


    });


    $(function() {
        app.router = new app.Router();
        Backbone.history.start();
    });


})();